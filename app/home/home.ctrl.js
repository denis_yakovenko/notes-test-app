app.controller('HomeCtrl', function(NotesService){
    this.items = NotesService.getNotes();
    this.deleteItem = function(index){
        NotesService.deleteNote(index);
    };

});