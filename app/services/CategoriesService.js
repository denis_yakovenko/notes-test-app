app
    .factory('CategoriesService', function ($localStorage, lodash) {
        var categories;
        var defaultCategories = [
                {
                    id: 1,
                    name: 'Home',
                    subcategories:[]
                },
                {
                    id: 2,
                    name: 'Work',
                    subcategories:[]
                },
                {
                    id: 3,
                    name: 'Hobby',
                    subcategories:[]
                },
                {
                    id: 4,
                    name: 'Health',
                    subcategories:[]
                }
            ];
        var lastCategoryId = $localStorage.lastCategoryId || 5;

        function deepFindId(arr, item, selected_value){
            var found = false;
            angular.forEach(arr, function(val){
                if(found)
                    return;
                if(item.id === val.id){
                    val.selected = selected_value;
                    found = true;
                }
                else if(val.subcategories && val.subcategories.length > 0)
                    deepFindId(val.subcategories, item, selected_value);
            });
        }
        var checkSelectedCategories = function(item_categories){
            angular.forEach(item_categories, function(item_category){
                deepFindId(categories, item_category, true);
            });
        };

        return {
            getLastId: function(){
                return lastCategoryId;
            },
            unSelectCategory: function(item){
                deepFindId(categories, item, false);
            },
            getCategories: function(item_categories){
                categories = angular.copy($localStorage.categories) || angular.copy(defaultCategories);
                checkSelectedCategories(item_categories);
                return categories;
            },
            addCategory: function(category, _lastCategoryId){
                lastCategoryId = _lastCategoryId;
                $localStorage.lastCategoryId = lastCategoryId;
                categories.push(angular.copy(category));
                $localStorage.categories = categories;
                return category;
            }
        }
    });