app
    .factory('TagsService', function ($localStorage, lodash) {
        var tags;
        var defaultTags = [
                {
                    id: 1,
                    name: 'High'
                },
                {
                    id: 2,
                    name: 'Medium'
                },
                {
                    id: 3,
                    name: 'Low'
                }
            ];
        var checkSelectedTags = function (itemTags) {
            lodash.forEach(itemTags, function (tag) {
                lodash.forEach(tags, function (val) {
                    if (val.id === tag.id)
                        val.selected = true;
                })
            });
        };
        return {
            getTags: function (itemTags) {
                tags = angular.copy($localStorage.tags) || angular.copy(defaultTags);
                checkSelectedTags(itemTags);
                return tags;
            },
            unSelectTag: function(tag){
                angular.forEach(tags, function(val){
                    if(val.id === tag.id)
                        val.selected = false;
                });

            },
            addNewTag: function (item_name) {
                var newTag = {
                    name: item_name,
                    id: tags[tags.length - 1].id + 1
                };
                tags.push(newTag);
                $localStorage.tags = tags;
                return newTag;
            }
        }
    });