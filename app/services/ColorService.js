app
    .factory('ColorService', function () {
        var colors = [
            {
                id: 1,
                color: '#dff0d8'
            },
            {
                id: 2,
                color: '#d9edf7'
            },
            {
                id: 3,
                color: '#fcf8e3'
            },
            {
                id: 4,
                color: '#f2dede'
            }
        ];
        return{
            getColors: function(){
                return colors;
            }
        }
    });