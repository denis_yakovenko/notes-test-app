app
    .service('NotesService', function ($localStorage, lodash) {
        var _this = this;
        this.notes = $localStorage.notes || [];
        this.create = function (item) {
            // set unique id
            item.id = (this.notes.length ? this.notes[this.notes.length - 1].id : 0) + 1;

            this.notes.push(item);
            $localStorage.notes = this.notes;
        };
        this.edit = function(item){
            angular.forEach(this.notes, function(val, k){
                if(item.id === val.id){
                    _this.notes[k] = item;
                }
            });
        };
        this.submitItem = function(item){
            if(!item.id)
                this.create(item);
            else
                this.edit(item);
        };
        this.getNotes = function () {
            return this.notes;
        };
        this.getNote = function(id){
            return lodash.find(this.notes, {id: +id});
        };
        this.deleteNote = function(index){
            this.notes.splice(index, 1);
        }
    });