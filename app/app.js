var app = angular.module('testapp',[
    'templates',
    'ui.router',
    'ngStorage',
    'ngLodash'
])

    //config
    .constant('APP_CONFIG', {
        app_name: 'testapp_'
    });