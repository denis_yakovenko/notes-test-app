app.directive('addCategoryModal', function(CategoriesService){
    return{
        restrict: 'E',
        templateUrl: 'note-detail/add-category.modal.html',
        link: function(scope, el, attr){
            var $modalEl = el.find('.modal');
            var emptySubcat = {name: ''};
            var lastCategoryId;


            scope.$watch(attr.showModal, function(newVal, oldVal){
                if(newVal === true){
                    $modalEl.modal('show');
                    lastCategoryId = CategoriesService.getLastId() + 1;
                    scope.newItem = {
                        name: '',
                        subcategories: [],
                        id: lastCategoryId
                    };
                }
            });

            scope.addSubcategory = function(){
                var subcat = angular.copy(emptySubcat);
                lastCategoryId++;
                subcat.id = lastCategoryId;
                scope.newItem.subcategories.push(subcat);
            };

            scope.removeSubcategory = function(index){
                scope.newItem.subcategories.splice(index, 1);
            };

            scope.checkSubcategoryIsEmpty = function(subcategory, index){
                if(scope.newItem.subcategories[index+1] && subcategory.name === '')
                    scope.removeSubcategory(index);
            };

            scope.createCategory = function(newCategory){
                scope.noteCtrl.addCategory(newCategory, lastCategoryId);
                $modalEl.modal('hide');
            };

            $modalEl.on('hide.bs.modal', function(){
                scope.newItem = null;
                scope.$applyAsync(function(){
                    scope.noteCtrl.showAddCategoryModal = false;
                });
            })
        }
    }
});