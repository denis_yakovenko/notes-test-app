app.controller('NoteDetailCtrl', function($state, NotesService, ColorService, TagsService, CategoriesService){

    this.colors = ColorService.getColors();
    var defaultNoteObj = { color: this.colors[0], tags: [], categories: []};
    this.item = $state.params.id ? angular.copy(NotesService.getNote($state.params.id)) : defaultNoteObj;
    this.tags = angular.copy(TagsService.getTags(this.item.tags));
    this.categories = angular.copy(CategoriesService.getCategories(this.item.categories));

    this.handleCreate = function($form, item){
        if(!$form.$valid)
            return;
        NotesService.submitItem(item);
        $state.go('main.home');
    };

    this.selectTag = function(e, tag_item){
        e.preventDefault();
        tag_item.selected = true;
        this.item.tags.push(tag_item);
    };

    this.selectCategory = function(e, category_item){
        e.preventDefault();
        category_item.selected = true;
        this.item.categories.push(category_item);
    };

    this.removeTag = function(index, tag){
        this.item.tags.splice(index, 1);
        TagsService.unSelectTag(tag);
    };

    this.removeCategory = function(index, category){
        this.item.categories.splice(index, 1);
        CategoriesService.unSelectCategory(this.categories, category);
    };

    this.addNewTag = function(tag_item){
        var newTag = angular.copy(TagsService.addNewTag(tag_item));
        this.tags.push(newTag);
    };

    this.addCategory = function(tag_item, lastCategoryId){
        CategoriesService.addCategory(tag_item, lastCategoryId);
        this.categories.push(tag_item);
    };
});