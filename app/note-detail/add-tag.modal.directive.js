app.directive('addTagModal', function(){
    return{
        restrict: 'E',
        templateUrl: 'note-detail/add-tag.modal.html',
        link: function(scope, el, attr){
            var $modalEl = el.find('.modal');
            scope.$watch(attr.showModal, function(newVal, oldVal){
                if(newVal === true)
                    $modalEl.modal('show');
            });
            scope.createTag = function(newTag){
                scope.noteCtrl.addNewTag(newTag);
                $modalEl.modal('hide');
            };
            $modalEl.on('hide.bs.modal', function(){
                scope.newTag = null;
                scope.$applyAsync(function(){
                    scope.noteCtrl.showAddTagModal = false;
                });
            })
        }
    }
});