app
    .config(function ($stateProvider, $urlRouterProvider) {

        $stateProvider

            .state('main', {
                abstract: true,
                url: '/',
                controller: 'MainCtrl as mc',
                templateUrl: 'main.html'

            })
            .state('main.home', {
                url: '',
                templateUrl: 'home/home.html',
                controller: 'HomeCtrl as homeCtrl'
            })
            .state('main.noteDetail', {
                url: 'note-detail/:id',
                templateUrl: 'note-detail/note-detail.html',
                controller: 'NoteDetailCtrl as noteCtrl'
            });

        $urlRouterProvider.otherwise('/');
    });